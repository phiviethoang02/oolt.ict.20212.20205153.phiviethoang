package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;
import java.lang.Math;

public class SolveEquations{
    static void linearEquation(){
        String s1,s2;
        JOptionPane.showMessageDialog(null,"ax + b =0","Input",JOptionPane.INFORMATION_MESSAGE);
        s1=JOptionPane.showInputDialog(null,"Enter a: ","Input",JOptionPane.INFORMATION_MESSAGE);
        double a= Double.parseDouble(s1);
        s2=JOptionPane.showInputDialog(null,"Enter b: ","Input",JOptionPane.INFORMATION_MESSAGE);
        double b= Double.parseDouble(s2);
        JOptionPane.showMessageDialog(null,
                                    "Solution: " + (-b/a),
                                    "Result",
                                    JOptionPane.INFORMATION_MESSAGE);
    }
    static void linearSystem(){
        double a[]= new double[6]; 
        for(int i = 0; i < 6;i++){
            String input = JOptionPane.showInputDialog(null,
             "a1x1 + a2x2 = a3\na4x1 + a5x2 = a6" + "\nInput a" + (i+1),
              null,
              JOptionPane.INFORMATION_MESSAGE);
            a[i] = Double.parseDouble(input);
        }
        double D[]= new double[3];
        D[0] = a[0]*a[4] - a[1]*a[3];
        D[1] = a[2]*a[4] - a[5]*a[1];
        D[2] = a[0]*a[5] - a[2]*a[3];
        if(D[0] != 0){
            JOptionPane.showMessageDialog(null,
                                        "The system has a unique solution: " + "x1 = " + (D[1]/D[0]) +
                                        ", " +"x2 = " + (D[2]/D[0]),
                                        "Result",
                                        JOptionPane.INFORMATION_MESSAGE);
        }
        else{
            if((D[1] == 0) && (D[2] == 0)){
                JOptionPane.showMessageDialog(null,
                                            "The system has infinitely many solutions",
                                            "Result",
                                            JOptionPane.INFORMATION_MESSAGE);
            }                                
            else{
                JOptionPane.showMessageDialog(null,
                                            "The system has no solution",
                                            "Result",
                                            JOptionPane.INFORMATION_MESSAGE);
            }                                
        }
    }
    static void secondEquation(){
        JOptionPane.showMessageDialog(null,"ax^2 + bx +c =0","Input",JOptionPane.INFORMATION_MESSAGE);  
        String s1= JOptionPane.showInputDialog(null, "Enter a: ","Input",JOptionPane.INFORMATION_MESSAGE);
        double a= Double.parseDouble(s1);
        String s2= JOptionPane.showInputDialog(null, "Enter b: ","Input",JOptionPane.INFORMATION_MESSAGE);
        double b= Double.parseDouble(s2);
        String s3= JOptionPane.showInputDialog(null, "Enter c: ","Input",JOptionPane.INFORMATION_MESSAGE);
        double c= Double.parseDouble(s3);
        double delta = Math.pow(b,2)- 4 * a * c;
        if(delta == 0){
            double root = -b/(2 * a);
            JOptionPane.showMessageDialog(null,
                                        "The equation has double root: x1 = x2 =" + root,
                                        "Result",
                                        JOptionPane.INFORMATION_MESSAGE);
        }else if(delta > 0){
            double root1 = (- b + Math.sqrt(delta))/(2 * a);
            double root2 = (- b - Math.sqrt(delta))/(2 * a);
            JOptionPane.showMessageDialog(null,
                                            "The equation has two distinct roots: " +
                                            "x1 = " + root1 + " and " +" x2 = " +root2,
                                            "Result",
                                            JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null,
                                            "The equation has no solution",
                                            "Result",
                                            JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public static void main(String[] args){
        String choice;
        choice = JOptionPane.showInputDialog(null, 
        "Enter choice: \n1.Solve first-degree equation\n2.Solve system of first-degree equations\n3.Solve second-degree equation\n4.Exit",
        "Menu",
        JOptionPane.INFORMATION_MESSAGE);
        int a= Integer.parseInt(choice);
        switch (a) {
            case 1:
                linearEquation();
                break;
        
            case 2:
                linearSystem();
                break;
            case 3:
                 secondEquation();
                break; 
            case 4:
                System.exit(0);       
        }
    }
}