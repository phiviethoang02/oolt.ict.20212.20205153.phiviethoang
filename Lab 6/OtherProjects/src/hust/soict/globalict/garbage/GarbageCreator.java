package hust.soict.globalict.garbage;

import java.io.File;
import java.util.*;

public class GarbageCreator {
    public static void main(String[] args) throws Exception {
        File file = new File("text.txt");
        Scanner r = new Scanner(file);
        long start = System.currentTimeMillis();
        String s = "";
        while (r.hasNextLine())
            s += r.nextLine();
        System.out.println(System.currentTimeMillis() - start);
        r.close();
    }
}