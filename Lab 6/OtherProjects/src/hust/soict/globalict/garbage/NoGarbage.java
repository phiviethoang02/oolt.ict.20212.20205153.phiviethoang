package hust.soict.globalict.garbage;
import java.io.File;
import java.util.*;

public class NoGarbage {
    public static void main(String[] args) throws Exception {
        File file = new File("text.txt");
        Scanner r = new Scanner(file);
        long start = System.currentTimeMillis();
        String s = "";
        StringBuilder sb = new StringBuilder();
        while (r.hasNextLine())
            sb.append(r.nextLine());
        s = sb.toString();
        System.out.println(System.currentTimeMillis() - start);
        r.close();
    }
}
