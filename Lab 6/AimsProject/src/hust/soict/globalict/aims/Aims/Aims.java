package hust.soict.globalict.aims.Aims;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order.Order;

public class Aims {

	public static void showMenu() {

		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");

	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice;
		int orderId;
		ArrayList<Order> orderList = new ArrayList<>();
		ArrayList<Media> itemsList = new ArrayList<>();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory ("Animation");
		dvd1.setCost (19.95f);
		dvd1.setDirector ("Roger Allers");
		dvd1.setLength (87);
		dvd1.setId(1);
		itemsList.add(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory ("Science Fiction");
		dvd2.setCost (24.95f);
		dvd2.setDirector ("George Lucas");
		dvd2.setLength (124);
		dvd2.setId(2);
		itemsList.add(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory ("Animation");
		dvd3.setCost (18.99f);
		dvd3.setDirector ("John Musker");
		dvd3.setLength (90);
		dvd3.setId(3);
		itemsList.add(dvd3);
		
		Book b1 = new Book("Doraemon","Comics");
		b1.addAuthor("Fujiko");
		b1.setCost(15.00f);
		b1.setId(4);
		itemsList.add(b1);
		
		Book b2 = new Book("Tat den","Roman");
		b2.addAuthor("Ngo Tat To");
		b2.setCost(20.00f);
		b2.setId(5);
		itemsList.add(b2);
		
		Book b3 = new Book("One Piece","Comic");
		b3.addAuthor("Oda Eiichiro");
		b3.setCost(15.5f);
		b3.setId(6);
		itemsList.add(b3);
		
		while (true) {
        	showMenu();
        	choice = sc.nextInt();
        	switch (choice) {
        		case 0:
        			sc.close();
        			return;
				case 1: 
					Order newOrder = new Order();
					orderList.add(newOrder);
					System.out.println("Order has been created");
					break;
				case 2:
					boolean notSucceed = true;
					System.out.print("Enter order's id: ");
					orderId = sc.nextInt();
					if (orderId > orderList.size()) System.out.println("Error: not exist");
					System.out.print("Enter item's id: ");
					int itemId = sc.nextInt();
					if (itemId > itemsList.size()) System.out.println("Error: not exist");
					for (Media e : itemsList) {
						if (e.getId() == itemId) {
							orderList.get(orderId-1).addMedia(e);
							System.out.println("Item was added successfully");
							notSucceed = false;
							break;
						}
					}
					if (notSucceed) System.out.println("Fail to add item");
					break;
				case 3:
					boolean notSucceed1 = true;
					System.out.print("Enter order's id: ");
					orderId = sc.nextInt();
					if (orderId > orderList.size()) System.out.println("Error: not exist");
					System.out.print("Enter item's id: ");
					int itemId1 = sc.nextInt();
					for (Media e : itemsList) {
						if (e.getId() == itemId1) {
							orderList.get(orderId-1).removeMedia(e);
							System.out.println("Item was removed successfully");
							notSucceed1 = false;
							break;
						}
					}
					if (notSucceed1) System.out.println("Fail to remove item");
					break;
				case 4:
					System.out.print("Enter order's id: ");
					orderId = sc.nextInt();
					orderList.get(orderId-1).print();
					break;
				default:
					System.out.println("Please enter a number between 0-4");
					break;
        	}
		}
	}
}
