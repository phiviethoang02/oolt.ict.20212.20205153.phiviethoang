package hust.soict.globalict.aims.order.Order;
import java.util.ArrayList;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private MyDate dateOrdered;
	public static final int MAX_LIMITTED_ORDERS = 5;
    private static int nbOrders = 0;
	private int qtyOrdered = 0;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	
	public Order(){
        dateOrdered = new MyDate();
        if(nbOrders<MAX_LIMITTED_ORDERS){
            nbOrders += 1;
        }else{
            throw new Error("The number of orders is full");
        }
    }
	
	public void addMedia(Media newItem) {
        if (itemsOrdered.size() == MAX_NUMBERS_ORDERED) {
            System.out.println("Max number of items reached!");
        } else {
            itemsOrdered.add(newItem);
            System.out.println("Item added successfully.");
        }
    }

	public void removeMedia(Media media) {
		if (itemsOrdered.remove(media)) System.out.println("Remove " + media.getTitle() + " successfully");
		else System.out.println("Fail to remove " + media.getTitle());
	}
	
	public float totalCost() {
		float sum = 0;
		for (Media disc : itemsOrdered) {
			if (disc!=null)
				sum += disc.getCost();
		}
		return sum;
		
	}
	
	public void print() {
		System.out.printf("***********************Order *************************\n");
		System.out.println("Date: " + dateOrdered);
		System.out.println("Ordered Items:");
		for (int i = 0; i < itemsOrdered.size(); i++) {
            System.out.println((i + 1) +" "+ itemsOrdered.get(i).getTitle() + " - " + itemsOrdered.get(i).getCategory()+ ": " + itemsOrdered.get(i).getCost() + "$");
        }
		System.out.printf("Total cost: %.2f$\n", totalCost());
		System.out.println("*****************************************************");
	}
	
	public Media getALuckyItem() {
		int freeDiscIndex = (int) Math.random() * qtyOrdered;
		Media freeDisc = itemsOrdered.get(freeDiscIndex);
		return freeDisc;
	}
}
