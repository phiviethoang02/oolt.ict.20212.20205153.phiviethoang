package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Media {
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super(director);
		this.director = director;
		this.length = length;
	}
	public DigitalVideoDisc(String title) {
		super(title);
	}
	
	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}
	
	public DigitalVideoDisc(String title, String category, String director) {
		super(title, category);
		this.director = director;
	}
	public DigitalVideoDisc(String title, String category, float cost) {
		super(title, category, cost);
	}
	private String director;
	private int length;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	public String toString() {
		return "DVD " + this.getId() + " - " + this.getTitle() + " - " + this.getCategory() + " - " + this.getDirector() + " - "+ this.getLength() + " - " + this.getCost() + "$";
	}
	
	public boolean search(String title) {
		if (this.getTitle().contains(title)) return true;
		return false;
	}
	
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
}
