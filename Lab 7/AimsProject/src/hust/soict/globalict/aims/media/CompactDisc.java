package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable {
	
	private String artist;
	private ArrayList<Track> tracks;
	
	public CompactDisc() {
		super();
		tracks = new ArrayList<>();
	}

	public CompactDisc(String title, String category, float cost) {
		super(title, category, cost);
		tracks = new ArrayList<>();
	}

	public CompactDisc(String title, String category, String director, int length, float cost) {
		super(title, category, director, length, cost);
		tracks = new ArrayList<>();
	}

	public CompactDisc(String title, String category) {
		super(title, category);
		tracks = new ArrayList<>();
	}

	public CompactDisc(String title) {
		super(title);
		tracks = new ArrayList<>();
	}
	
	public String getArtist() {
		return artist;
	}
	

	public int getLength() {
		if (tracks.size() == 0) return 0;
		int totalLength = 0;
		for (Track e : tracks)
			if (e != null) totalLength += e.getLength();			
		return totalLength;
	}
	
	public void addTrack(Track track) {
		if (tracks.contains(track)) 
			System.out.println("Track " + track.getTitle() + " was already in track list");
		else {
			tracks.add(track);
			System.out.println("Track " + track.getTitle() + " was added");
		}
	}
	
	public void removeTrack(Track track) {
		if (tracks.remove(track)) 
			System.out.println("Track " + track.getTitle() + " was removed");
		else 
			System.out.println("Track " + track.getTitle() + " wasn't in track list");
	}
	

	public String toString() {
		return "CD " + this.getId() + " - " + this.getTitle() + " - " + this.getCategory() + " - " + this.getDirector() + " - " + this.getArtist() + " - " + this.getLength() + " - " + this.getCost() + "$";
	}


	public void play() {
		if (tracks.size() == 0) {
			System.out.println("No track to play");
			return;
		}
		for (Track e : tracks)
			e.play();
	}
}