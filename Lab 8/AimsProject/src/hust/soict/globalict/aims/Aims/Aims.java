package hust.soict.globalict.aims.Aims;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Playable;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order.Order;
import hust.soict.globalict.aims.thread.MemoryDaemon;

public class Aims {

	
	
	public static void showMenu() {

		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("5. Play item");
		System.out.println("6. Add track for CDs");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");

	}
	
	public static void main(String[] args) {
		MemoryDaemon memory = new MemoryDaemon();
		Thread thread = new Thread(memory);
		thread.setDaemon(true);
		thread.start();
		
		Scanner sc = new Scanner(System.in);
		int choice;
		int orderId;
		ArrayList<Order> orderList = new ArrayList<>();
		ArrayList<Media> itemsList = new ArrayList<>();
		
//		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
//		dvd1.setCategory ("Animation");
//		dvd1.setCost (19.95f);
//		dvd1.setDirector ("Roger Allers");
//		dvd1.setLength (87);
//		dvd1.setId(1);
//		itemsList.add(dvd1);
//		
//		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
//		dvd2.setCategory ("Science Fiction");
//		dvd2.setCost (24.95f);
//		dvd2.setDirector ("George Lucas");
//		dvd2.setLength (124);
//		dvd2.setId(2);
//		itemsList.add(dvd2);
//		
//		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
//		dvd3.setCategory ("Animation");
//		dvd3.setCost (18.99f);
//		dvd3.setDirector ("John Musker");
//		dvd3.setLength (90);
//		dvd3.setId(3);
//		itemsList.add(dvd3);
//		
//		Book b1 = new Book("Doraemon","Comics");
//		b1.addAuthor("Fujiko");
//		b1.setCost(15.00f);
//		b1.setId(4);
//		itemsList.add(b1);
//		
//		Book b2 = new Book("Tat den","Roman");
//		b2.addAuthor("Ngo Tat To");
//		b2.setCost(20.00f);
//		b2.setId(5);
//		itemsList.add(b2);
//		
//		Book b3 = new Book("One Piece","Comic");
//		b3.addAuthor("Oda Eiichiro");
//		b3.setCost(15.5f);
//		b3.setId(6);
//		itemsList.add(b3);
		
		while (true) {
        	showMenu();
        	choice = sc.nextInt();
        	switch (choice) {
        		case 0:
        			sc.close();
        			return;
				case 1: 
					Order newOrder = new Order();
					orderList.add(newOrder);
					System.out.println("Order has been created");
					break;
				case 2:
					int num=0;
					System.out.println("1. CompactDisc");
					System.out.println("2. DigitalVideoDisc");
					System.out.println("3. Book");
					System.out.print("Enter your option: ");
					int type = sc.nextInt();
					if (type == 1) {						
						System.out.print("Enter order's id: ");
						orderId = sc.nextInt();
						if (orderId > orderList.size()) System.out.println("Error: not exist");
						System.out.print("How many items do you want to add ");
						int n = sc.nextInt();
						for(int i=0;i<n;i++) {
							sc = new Scanner(System.in);
							System.out.print("Enter tilte: ");
							String title = sc.nextLine();							
							System.out.print("Enter category: ");
							String category = sc.nextLine();
							System.out.print("Enter cost: ");
							float cost = sc.nextFloat();
							CompactDisc newItem = new CompactDisc(title, category, cost);
							num=num+1;
							newItem.setId(num);
							itemsList.add(newItem);
							System.out.println("Item has been created");
							orderList.get(orderId-1).addMedia(newItem);
							System.out.println("Item was added successfully");							
						}
					}
					else if (type == 2) {
						System.out.print("Enter order's id: ");
						orderId = sc.nextInt();
						if (orderId > orderList.size()) System.out.println("Error: not exist");
						System.out.print("How many items do you want to add ");
						int n = sc.nextInt();
						for(int i=0;i<n;i++) {
							sc = new Scanner(System.in);
							System.out.print("Enter tilte: ");
							String title = sc.nextLine();
							System.out.print("Enter category: ");
							String category = sc.nextLine();
							System.out.print("Enter director: ");
							String director = sc.nextLine();
							System.out.print("Enter length: ");
							int length = sc.nextInt();
							System.out.print("Enter cost: ");
							float cost = sc.nextFloat();
							DigitalVideoDisc newItem = new DigitalVideoDisc(title, category, cost);
							newItem.setDirector(director);
							newItem.setLength(length);
							num=num+1;
							newItem.setId(num);
							itemsList.add(newItem);
							System.out.println("Item has been created");
							orderList.get(orderId-1).addMedia(newItem);
							System.out.println("Item was added successfully");
						}
					}
					else if (type == 3) {
						System.out.print("Enter order's id: ");
						orderId = sc.nextInt();
						if (orderId > orderList.size()) System.out.println("Error: not exist");
						System.out.print("How many items do you want to add ");
						int n = sc.nextInt();
						for(int i=0;i<n;i++) {
							sc = new Scanner(System.in);
							System.out.print("Enter title: ");
							String title = sc.nextLine();
							System.out.print("Enter category: ");
							String category = sc.nextLine();
							System.out.print("Enter authors: ");
							String authorName = sc.nextLine();
							List<String> authors = Arrays.asList(authorName.split(", "));
							System.out.print("Enter content: ");
							String content = sc.nextLine();						
							System.out.print("Enter cost: ");
							float cost = sc.nextFloat();
							Book newItem = new Book(title, category, authors, cost);
							newItem.setContent(content);
							num=num+1;
							newItem.setId(num);
							itemsList.add(newItem);
							System.out.println("Item has been created");
							orderList.get(orderId-1).addMedia(newItem);
							System.out.println("Item was added successfully");
						}
					}
					break;
				case 3:
					boolean notSucceed1 = true;
					System.out.print("Enter order's id: ");
					orderId = sc.nextInt();
					if (orderId > orderList.size()) 
						System.out.println("Error: not exist");
					System.out.print("Enter item's id: ");
					int itemId1 = sc.nextInt();
					for (Media e : itemsList) {
						if (e.getId() == itemId1) {
							orderList.get(orderId-1).removeMedia(e);
							System.out.println("Item was removed successfully");
							notSucceed1 = false;
							break;
						}
					}
					if (notSucceed1) 
						System.out.println("Fail to remove item");
					break;
				case 4:
					System.out.print("Enter order's id: ");
					orderId = sc.nextInt();
					orderList.get(orderId-1).print();
					break;					
				case 5:
					System.out.print("Enter item's id: ");
					int itemId = sc.nextInt();
					boolean succeed = false;
					for (Media e : itemsList) {
						if (e.getId() == itemId && e instanceof Playable) {
							Playable playableThing = (Playable) e;
							playableThing.play();
							succeed = true;
							break;
						}
					}
					if (!succeed) 
						System.out.println("Error: cannot find any playable items");
					break;
				case 6:
					sc.nextLine();
					System.out.print("Enter track'name: ");
					String title = sc.nextLine();
					System.out.print("Enter track's length: ");
					int length = sc.nextInt();
					Track newTrack = new Track(title, length);
					System.out.print("Enter CompactDisc's id: ");
					int compactDiscId = sc.nextInt();
					boolean succeed1 = false;
					for (Media e : itemsList)
					{
						if (e.getId() == compactDiscId && e instanceof CompactDisc)
						{
							CompactDisc cd = (CompactDisc) e;
							cd.addTrack(newTrack);
							System.out.println("Track " + newTrack.getTitle() + " was add to CD " + e.getId());
							succeed1 = true;
							break;
						}
					}
					if (!succeed1) System.out.println("Error: cannot find any CompactDisc");
					break;
				default:
					System.out.println("Please enter a number between 0-4");
					break;
        	}
		}
	}
}
