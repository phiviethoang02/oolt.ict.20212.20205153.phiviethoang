package hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate.MyDate;

public class DateUtils {
	public static int Compare(MyDate date1, MyDate date2) {
		return (date1.getYear()-date2.getYear())*365 + (date1.getMonth()-date2.getMonth())*31 + (date1.getDay()-date2.getDay()); 
	}
	
	public static void dateCopy(MyDate day2, MyDate day1) {
		day2.setDay(day1.getDay());
		day2.setMonth(day1.getMonth());
		day2.setYear(day1.getYear());
	}
	
	public static void sortDates(MyDate[] dates) {
		MyDate temp = new MyDate();
		for (int i = 0; i < dates.length; i++)
			for (int j = 0; j < dates.length - 1 - i; j++)
				if (Compare(dates[j], dates[j+1]) > 0) {
					dateCopy(temp, dates[j]);
					dateCopy(dates[j], dates[j+1]);
					dateCopy(dates[j+1], temp);
				}		
	}
	
	public static void dateCompare(MyDate date1, MyDate date2) {
		int n = Math.abs(date1.getYear()-date2.getYear())*365 + (date1.getMonth()-date2.getMonth())*31 + (date1.getDay()-date2.getDay()); 
		if (Compare(date1, date2)<0) System.out.println("First Date > Second Date: "+ n + "days");
		else if (Compare(date1, date2)==0) System.out.println("First Date = Second Date: ");
		else System.out.println("First Date < Second Date: "+ n + "days");
	}
}