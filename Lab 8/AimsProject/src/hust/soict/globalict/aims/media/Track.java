package hust.soict.globalict.aims.media;

public class Track implements Playable, Comparable<Track> {
	
	private String title;
	private int length;
	
	public Track() {
		// TODO Auto-generated constructor stub
	}
	
	public Track(String title, int length) {
		this.title = title;
		this.length = length;
	}

	public String getTitle() {
		return title;
	}

	public int getLength() {
		return length;
	}

	public void play() {
		System.out.println("Playing track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
	}

	@Override
	public int compareTo(Track o) {
		return (int)(this.getLength() - o.getLength());
	}
	
}
