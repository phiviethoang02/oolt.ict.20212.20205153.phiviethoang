package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Book extends Media implements Comparable<Book>{
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new TreeMap<>();
	public Book(String title) {
		super(title);
	}
	public Book(String title, String category) {
		super(title, category);
	}
	
	public Book(String title, String category, List<String> authors){
		super(title, category);
		this.authors = authors;
	}
	
	public Book(String title, String category, List<String> authors, float cost){
		super(title, category, cost);
		this.authors = authors;
	}
	
	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}
	public String getTitle() {
		return super.getTitle();
	}

	public void setTitle(String title) {
		super.setTitle(title);
	}

	public String getCategory() {
		return super.getCategory();
	}

	public void setCategory(String category) {
		super.setCategory(category);
	}

	public float getCost() {
		return super.getCost();
	}

	public void setCost(float cost) {
		super.setCost(cost);
	}
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String nameOfAuthor) {
		if (this.authors.contains(nameOfAuthor)) {
			System.out.println("This author is already in the author list!");
			return;
		}
		if (this.authors.add(nameOfAuthor)) System.out.println("Add successfully!");
		else System.out.println("Fail to add!");
	}
	
	public void removeAuthor(String nameOfAuthor) {
		if (!this.authors.contains(nameOfAuthor)) {
			System.out.println("This name is not in the author list!");
			return;
		}
		if (this.authors.remove(nameOfAuthor)) 
			System.out.println("Remove successfully!");
		else System.out.println("Fail to remove!");
	}
	
	@Override
	public int compareTo(Book o) {
		return (int)(this.getCost() - o.getCost());
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}
	
	public int getContentLength() {
		return contentTokens.size();
	}
	
	public List<String> getContentTokens() {
		return contentTokens;
	}

	public Map<String, Integer> getWordFrequency() {
		return wordFrequency;
	}
	
	public void processContent(){
        wordFrequency = new TreeMap<>();
        String[] array = content.split(" ");

        for (String e : array) {
            wordFrequency.putIfAbsent(e, 0);
            wordFrequency.put(e, (wordFrequency.get(e) + 1));
        }

        contentTokens = new ArrayList<String>(wordFrequency.keySet());
    }

    @Override
    public String toString() {
        String output = super.toString();
        output += "\n   Content: " + content + "\n   TokenList: " + contentTokens + "\n   Frequency: " + wordFrequency ;
        return output;
    }

}
