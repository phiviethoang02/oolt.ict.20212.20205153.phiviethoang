package hust.soict.globalict.test.utils.DateTest;
import hust.soict.globalict.aims.utils.DateUtils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate.MyDate;

public class DateTest {

	public static void main(String[] args) {
		MyDate date1 = new MyDate();
		date1.print("yyyy-MMM-dd");
		MyDate date2 = new MyDate(14, 12, 2002);
		date2.print("dd-MMM-yyyy");
		MyDate date3 = new MyDate("second", "February", "two thousand and two");
		date3.print("dd/mm/yyyy");
		MyDate date4 = new MyDate("March 3rd 2002");
		date4.print("mmm dd yyyy");
		
		
		DateUtils.dateCompare(date2,date3);
		
		MyDate[] dates = {date1, date2, date3, date4};

		DateUtils.sortDates(dates);
		for (int i = 0; i < dates.length; i++)
			dates[i].print();
	}
	

}