public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
    
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered < MAX_NUMBERS_ORDERED) {
			for (int i = 0; i < MAX_NUMBERS_ORDERED; i++) {
				if (itemsOrdered[i] == null) {
					itemsOrdered[i] = disc;
					qtyOrdered++;
					System.out.println("The disc has been added.");
					if (qtyOrdered == MAX_NUMBERS_ORDERED) {
						System.out.println("The order is almost full");
					}
				    break;
				}
			}		
		}
		else System.out.println("Error: The disc has not been added");
	}
	
	public void removeDigitalVideoDisc (DigitalVideoDisc disc) {
		for(int i=0;i<qtyOrdered;i++){
            if(itemsOrdered[i] == disc){
                System.arraycopy(itemsOrdered, i + 1, itemsOrdered, i, qtyOrdered - i);
                System.out.println("The disc has been removed");
                qtyOrdered --;
                return;
            }
        }
        System.out.println("ERROR!");
	}
	
	public float totalCost() {
		float sum = 0;
		for (DigitalVideoDisc disc : itemsOrdered) {
			if (disc!=null)
				sum += disc.getCost();
		}
		return sum;
		
	}
}
