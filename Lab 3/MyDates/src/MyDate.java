
import java.util.Scanner;

public class MyDate {
	private int day;
	private int month;
	private int year;
	public MyDate() {
		super();
	}
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day>0&&day<=31)
			this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month>0&&month<=12)
			this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String dateString) {
		String[] parts = dateString.split(" ");
		String monthString = parts[0];
		String dayString = parts[1];
		String yearString = parts[2];
		//set day
		if (dayString.length() == 3) {
			this.day = Integer.parseInt(dayString.substring(0, 1));
		}
		else if (dayString.length() == 4) {
			this.day = Integer.parseInt(dayString.substring(0, 2));
		}
		else
			this.day = 0;
		//set month
		if (monthString.equalsIgnoreCase("January")) {
			this.month = 1;
		}
		else if (monthString.equalsIgnoreCase("February")) {
			this.month = 2;
		}
		else if (monthString.equalsIgnoreCase("March")) {
			this.month = 3;
		}
		else if (monthString.equalsIgnoreCase("April")) {
			this.month = 4;
		}
		else if (monthString.equalsIgnoreCase("May")) {
			this.month = 5;
		}
		else if (monthString.equalsIgnoreCase("June")) {
			this.month = 6;
		}
		else if (monthString.equalsIgnoreCase("July")) {
			this.month = 7;
		}
		else if (monthString.equalsIgnoreCase("August")) {
			this.month = 8;
		}
		else if (monthString.equalsIgnoreCase("September")) {
			this.month = 9;
		}
		else if (monthString.equalsIgnoreCase("October")) {
			this.month = 10;
		}
		else if (monthString.equalsIgnoreCase("November")) {
			this.month = 11;
		}
		else if (monthString.equalsIgnoreCase("December")) {
			this.month = 12;
		}
		else
			this.month = 0;
		//set year
		this.year = Integer.parseInt(yearString);
	}

	
	public void accept() {
		System.out.print("Enter a date (e.g: October 11th 2002): ");
		Scanner input = new Scanner(System.in);
		String dateString = input.nextLine();
		String[] parts = dateString.split(" ");
		String monthString = parts[0];
		String dayString = parts[1];
		String yearString = parts[2];
		//set day
		if (dayString.length() == 3) {
			this.day = Integer.parseInt(dayString.substring(0, 1));
		}
		else if (dayString.length() == 4) {
			this.day = Integer.parseInt(dayString.substring(0, 2));
		}
		else
			this.day = 0;
		
		if (monthString.equalsIgnoreCase("January")) {
			this.month = 1;
		}
		else if (monthString.equalsIgnoreCase("February")) {
			this.month = 2;
		}
		else if (monthString.equalsIgnoreCase("March")) {
			this.month = 3;
		}
		else if (monthString.equalsIgnoreCase("April")) {
			this.month = 4;
		}
		else if (monthString.equalsIgnoreCase("May")) {
			this.month = 5;
		}
		else if (monthString.equalsIgnoreCase("June")) {
			this.month = 6;
		}
		else if (monthString.equalsIgnoreCase("July")) {
			this.month = 7;
		}
		else if (monthString.equalsIgnoreCase("August")) {
			this.month = 8;
		}
		else if (monthString.equalsIgnoreCase("September")) {
			this.month = 9;
		}
		else if (monthString.equalsIgnoreCase("October")) {
			this.month = 10;
		}
		else if (monthString.equalsIgnoreCase("November")) {
			this.month = 11;
		}
		else if (monthString.equalsIgnoreCase("December")) {
			this.month = 12;
		}
		else
			this.month = 0;	
		this.year = Integer.parseInt(yearString);
		input.close();
	}
	
	public void print() {
		String dateString = "";
		dateString += Integer.toString(day);
		dateString += '/';
		dateString += Integer.toString(month);
		dateString += '/';
		dateString += Integer.toString(year);
		System.out.println(dateString);
	}
}
