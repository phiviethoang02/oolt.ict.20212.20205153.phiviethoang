package hust.soict.globalict.gui.awt;

import java.awt.*; 
import java.awt.event.*; 

public class AWTCounter extends Frame implements ActionListener {
	
	private Label lblCount; 
	private TextField tfCount; 
	private Button btnCount; 
	private int count = 0; 
	

	public AWTCounter () {
		setLayout(new FlowLayout());

		lblCount = new Label("Counter"); // 
		add(lblCount); 
		
		tfCount = new TextField(count + "", 10);
		tfCount.setEditable(false);
		add(tfCount);
		
		btnCount = new Button("Count");
		add(btnCount); 
		
		btnCount.addActionListener(this);
		addWindowListener(new MyWindowListener());
		
		setTitle("AWT Counter");
		setSize(250, 100);
		
		
		setVisible(true); 
		}

	public static void main(String[] args) {

		AWTCounter app = new AWTCounter();
		

	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		++count; 
		tfCount.setText(count + ""); 
	}
	
	public void windowClosing(WindowEvent evt) {
        System.exit(0);  // Terminate the program
     }
	private class MyWindowListener implements WindowListener {
	      // Called back upon clicking close-window button
	      @Override
	      public void windowClosing(WindowEvent evt) {
	         System.exit(0);  // Terminate the program
	      }

	      // Not Used, BUT need to provide an empty body to compile.
	      @Override public void windowOpened(WindowEvent evt) { }
	      @Override public void windowClosed(WindowEvent evt) { }
	      // For Debugging
	      @Override public void windowIconified(WindowEvent evt) { System.out.println("Window Iconified"); }
	      @Override public void windowDeiconified(WindowEvent evt) { System.out.println("Window Deiconified"); }
	      @Override public void windowActivated(WindowEvent evt) { System.out.println("Window Activated"); }
	      @Override public void windowDeactivated(WindowEvent evt) { System.out.println("Window Deactivated"); }
	}
}