

public class Aims {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Order anOrder = new Order();
		// Create a new dvd object and set the fields
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory ("Animation");
		dvd1.setCost (19.95f);
		dvd1.setDirector ("Roger Allers");
		dvd1.setLength (87);
		// add the dvd to the order
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory ("Science Fiction");
		dvd2.setCost (24.95f);
		dvd2.setDirector ("George Lucas");
		dvd2.setLength (124);
		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory ("Animation");
		dvd3.setCost (18.99f);
		dvd3.setDirector ("John Musker");
		dvd3.setLength (90);
		
		// add the dvd to the order
		anOrder.addDigitalVideoDisc(dvd3);
		
		System.out.print ("Total Cost is: ");
		System.out.println (anOrder.totalCost());
		
	    System.out.println("Remove Star Wars from the order");
	    anOrder.removeDigitalVideoDisc(dvd1);
	    System.out.println("Total cost after removing : " + anOrder.totalCost());
	    anOrder.addDigitalVideoDisc(dvd1);
	    
	    // add the dvd1, dvd2 and dvd3 using addDigitalVideoDisc(DigitalVideoDisc [] dvdList)
	    /*DigitalVideoDisc [] dvdList = {dvd1, dvd2, dvd3};
	    anOrder.addDigitalVideoDisc(dvdList);
	    System.out.println("Total cost after adding : " + anOrder.totalCost()); */
	    
	    // add the dvd1 and dvd2 using addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2)
	    /*anOrder.addDigitalVideoDisc(dvd1,dvd2);
	    System.out.println("Total cost after adding : " + anOrder.totalCost());
	    anOrder.removeDigitalVideoDisc(dvd1);
	    anOrder.removeDigitalVideoDisc(dvd2);*/
	    
	    anOrder.print();
	}

}
