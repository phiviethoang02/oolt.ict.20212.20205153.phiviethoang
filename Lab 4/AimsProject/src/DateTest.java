
public class DateTest {

	public static void main(String[] args) {
		MyDate date1 = new MyDate();
		date1.print("yyyy-MMM-dd");
		MyDate date2 = new MyDate(14, 12, 2002);
		date2.print("dd-MMM-yyyy");
		MyDate date3 = new MyDate("March 3rd 2002");
		date3.print("mmm dd yyyy");
		MyDate date4 = new MyDate("second", "February", "two thousand and two");
		date4.print("dd/mm/yyyy");
		
		DateUtils.dateCompare(date2,date3);
		
		MyDate[] dates = {date1, date2, date3, date4};

		DateUtils.sortDates(dates);
		for (int i = 0; i < dates.length; i++)
			dates[i].print();
	}
	

}