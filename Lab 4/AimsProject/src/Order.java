

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private MyDate dateOrdered;
	public static final int MAX_LIMITTED_ORDERS = 5;
    private static int nbOrders = 0;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
    
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered < MAX_NUMBERS_ORDERED) {
			for (int i = 0; i < MAX_NUMBERS_ORDERED; i++) {
				if (itemsOrdered[i] == null) {
					itemsOrdered[i] = disc;
					qtyOrdered++;
					System.out.println("The disc has been added.");
					if (qtyOrdered == MAX_NUMBERS_ORDERED) {
						System.out.println("The order is almost full");
					}
				    break;
				}
			}		
		}
		else System.out.println("Error: The disc has not been added");
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		int n = dvdList.length;
		if (qtyOrdered + n <= MAX_NUMBERS_ORDERED) {					
			for (int i = 0; i<n; i++) {				
				itemsOrdered[qtyOrdered+i]=dvdList[i];
			}
		}else System.out.println("The order is almost full");
		
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
		if (qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("Error: The order is FULL, no dvd was added");
		}
		else if (qtyOrdered == MAX_NUMBERS_ORDERED-1) {
			this.addDigitalVideoDisc(dvd1);
			System.out.println("Error: The order is FULL, dvd2 was added");
		}
		else {
			this.addDigitalVideoDisc(dvd1);
			this.addDigitalVideoDisc(dvd2);
			System.out.println("Two dvds have been added");
		}
	}
	
	public Order(){
        dateOrdered = new MyDate();
        if(nbOrders<MAX_LIMITTED_ORDERS){
            nbOrders += 1;
        }else{
            throw new Error("The number of orders is full");
        }
    }
	
	
	public void removeDigitalVideoDisc (DigitalVideoDisc disc) {
		for(int i=0;i<qtyOrdered;i++){
            if(itemsOrdered[i] == disc){
                System.arraycopy(itemsOrdered, i + 1, itemsOrdered, i, qtyOrdered - i);
                System.out.println("The disc has been removed");
                qtyOrdered --;
                return;
            }
        }
        System.out.println("ERROR!");
	}
	
	public float totalCost() {
		float sum = 0;
		for (DigitalVideoDisc disc : itemsOrdered) {
			if (disc!=null)
				sum += disc.getCost();
		}
		return sum;
		
	}
	
	public void print() {
		System.out.printf("***********************Order *************************\n");
		System.out.println("Date: " + dateOrdered);
		System.out.println("Ordered Items:");
		for (int i = 0; i < qtyOrdered; i++) {
			System.out.printf("%d. DVD - %s - %s - %s - %d - %.2f$\n", i+1, itemsOrdered[i].getTitle(), itemsOrdered[i].getCategory(), itemsOrdered[i].getDirector(), itemsOrdered[i].getLength(), itemsOrdered[i].getCost());
		}
		System.out.printf("Total cost: %.2f$\n", totalCost());
		System.out.println("*****************************************************");
	}
}
