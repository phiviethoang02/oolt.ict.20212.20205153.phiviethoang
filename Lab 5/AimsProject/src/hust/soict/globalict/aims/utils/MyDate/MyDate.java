package hust.soict.globalict.aims.utils.MyDate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Scanner;
import java.lang.Character;


public class MyDate {
	private static final String[] days = {"","first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth",
            "eleventh", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twentieth",
            "twenty-first", "twenty-second", "twenty-third", "twenty-fourth", "twenty-fifth", "twenty-sixth", "twenty-seventh", "twenty-eighth", "twenty-ninth", "thirtieth",
            "thirtieth-one"};
	private static final String[] months = {"", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	private static final String[] units = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
	private static final String[] tens = {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
	private static final String[] big = {"", "", "hundred", "thousand"};
	private int day;
	private int month;
	private int year;
	
	public MyDate() {
		LocalDate currentDate = LocalDate.now();
		this.day = currentDate.getDayOfMonth();
		this.month = currentDate.getMonthValue();
		this.year = currentDate.getYear();
	}
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day>0&&day<=31)
			this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month>0&&month<=12)
			this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String dateString) {
		String[] parts = dateString.split(" ");
		String monthString = parts[0];
		String dayString = parts[1];
		String yearString = parts[2];
		//set day
		if (dayString.length() == 3) {
			this.day = Integer.parseInt(dayString.substring(0, 1));
		}
		else if (dayString.length() == 4) {
			this.day = Integer.parseInt(dayString.substring(0, 2));
		}
		else
			this.day = 0;
		//set month
		if (monthString.equals("January")) {
			this.month = 1;
		}
		else if (monthString.equals("February")) {
			this.month = 2;
		}
		else if (monthString.equals("March")) {
			this.month = 3;
		}
		else if (monthString.equals("April")) {
			this.month = 4;
		}
		else if (monthString.equals("May")) {
			this.month = 5;
		}
		else if (monthString.equals("June")) {
			this.month = 6;
		}
		else if (monthString.equals("July")) {
			this.month = 7;
		}
		else if (monthString.equals("August")) {
			this.month = 8;
		}
		else if (monthString.equals("September")) {
			this.month = 9;
		}
		else if (monthString.equals("October")) {
			this.month = 10;
		}
		else if (monthString.equals("November")) {
			this.month = 11;
		}
		else if (monthString.equals("December")) {
			this.month = 12;
		}
		else
			this.month = 0;
		this.year = Integer.parseInt(yearString);
	}

	
	public void accept() {
		System.out.print("Enter a date (e.g: December 14th 2002): ");
		Scanner input = new Scanner(System.in);
		String dateString = input.nextLine();
		String[] parts = dateString.split(" ");
		String monthString = parts[0];
		String dayString = parts[1];
		String yearString = parts[2];
		//set day
		if (dayString.length() == 3) {
			this.day = Integer.parseInt(dayString.substring(0, 1));
		}
		else if (dayString.length() == 4) {
			this.day = Integer.parseInt(dayString.substring(0, 2));
		}
		else
			this.day = 0;
		
		if (monthString.equals("January")) {
			this.month = 1;
		}
		else if (monthString.equals("February")) {
			this.month = 2;
		}
		else if (monthString.equals("March")) {
			this.month = 3;
		}
		else if (monthString.equals("April")) {
			this.month = 4;
		}
		else if (monthString.equals("May")) {
			this.month = 5;
		}
		else if (monthString.equals("June")) {
			this.month = 6;
		}
		else if (monthString.equals("July")) {
			this.month = 7;
		}
		else if (monthString.equals("August")) {
			this.month = 8;
		}
		else if (monthString.equals("September")) {
			this.month = 9;
		}
		else if (monthString.equals("October")) {
			this.month = 10;
		}
		else if (monthString.equals("November")) {
			this.month = 11;
		}
		else if (monthString.equals("December")) {
			this.month = 12;
		}
		else
			this.month = 0;	
		this.year = Integer.parseInt(yearString);
		input.close();
	}
	
	
	public MyDate(String day, String month, String year) {
		//set day
		this.day = Arrays.asList(days).indexOf(day);
		
		//set month
		this.month = Arrays.asList(months).indexOf(month);
		
		//set year
		this.year = 0;
		if (year.toLowerCase().contains("thousand") || year.toLowerCase().contains("hundred")) {
			String[] parts = year.split(" ");
			int result = 0;
			next:
			for (int i=0; i<parts.length; i++) {
				if (parts[i].equals("and")) continue;
				for (int j = 0; j < units.length; j++) {
					if (parts[i].equals(units[j])) {
						this.year += j;
						result = j;
						continue next;
					}
				}
				for (int j = 0; j < big.length; j++) {
					if (parts[i].equals(big[j])) {
						this.year -= result;
						result *= (int) Math.pow(10, j);
						this.year += result;
						continue next;
					}
				}		
			}
		}
		else {
			StringBuilder result = new StringBuilder("");
			int n = 0;
			String[] parts = year.split(" ");
			next:
			for (String e : parts) {
				for (int i = 0; i < units.length; i++) {
					if (e.equals(units[i])) {
						n += i;
						result.append(n);
						n = 0;
						continue next;
					}
				}
				for (int i = 0; i < tens.length; i++) {
					if (e.equals(tens[i])) {
						if (n > 0) result.append(n);
						n = i*10;
						continue next;
					}
				}
			}
			if (n > 0) result.append(n);
			this.year = Integer.parseInt(result.toString());
		}
	}
	
	public void print(String format) {
		StringBuilder dateString = new StringBuilder("");
		StringBuilder space = new StringBuilder("");
		for (int i = 0; i < format.length(); i++) {
			if (Character.isLetter(format.charAt(i)));
			else {
				space.append(format.charAt(i));
				break;
			}
		}
		String[] parts = format.split(space.toString());
		for (int i=0; i<parts.length; i++) {
			if (parts[i].toLowerCase().contains("d") && parts[i].length() == 1) 
				dateString.append(day);
			
			else if (parts[i].toLowerCase().contains("d") && parts[i].length() == 2) {
				if (day < 10) dateString.append('0');
				dateString.append(day);
			}
			
			if (parts[i].toLowerCase().contains("m") && parts[i].length() == 1) 
				dateString.append(month);
			
			else if (parts[i].toLowerCase().contains("m") && parts[i].length() == 2) {
				if (month < 10) dateString.append('0');
				dateString.append(day);
			}
			
			else if (parts[i].toLowerCase().contains("m") && parts[i].length() == 3) 
				dateString.append(months[month].substring(0, 3));
			
			if (parts[i].toLowerCase().contains("y") && parts[i].length() == 4) 
				dateString.append(year);
			dateString.append(space);
		}
		System.out.println(dateString.substring(0, dateString.length()-1));
	}
	
	
	
	public void print() {
		String monthString = null, dayString = null;
		//days
		if(day%10 == 1 && day != 11) 
			dayString = Integer.toString(day) + "st";
		else if(day%10 == 2 && day != 12) 
			dayString = Integer.toString(day) + "nd";
		else if(day%10 == 3 && day != 13) 
			dayString = Integer.toString(day) + "rd";
		else
			dayString = Integer.toString(day) + "th";
		
		//months
		if(month == 1) 
			monthString = "January";
		else if(month == 2)
			monthString = "February";
		else if(month == 3)
			monthString = "March";
		else if(month == 4)
			monthString = "April";
		else if(month == 5)
			monthString = "May";
		else if(month == 6)
			monthString = "June";
		else if(month == 7)
			monthString = "July";
		else if(month == 8)
			monthString = "August";
		else if(month == 9)
			monthString = "September";
		else if(month == 10)
			monthString = "October";
		else if(month == 11)
			monthString = "November";
		else if(month == 12)
			monthString = "December";

		System.out.println(monthString + " " + dayString + " " + year);
	}
	
	public String toString() {
		return LocalDate.of(year, month, day).toString();
	}
	
}

