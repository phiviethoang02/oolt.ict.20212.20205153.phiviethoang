package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;
public class Ex5 {

    public static void main(String[] args)
    {
        String strNum1, strNum2;
        String strNotification = "RESULT:\n";
        strNum1 = JOptionPane. showInputDialog(null,
                "Please input the first number: ","Input the first number",
                JOptionPane. INFORMATION_MESSAGE);

        strNum2 = JOptionPane. showInputDialog(null,
                 "Please input the second number: ","Input the second number",
                JOptionPane. INFORMATION_MESSAGE);

        double num1 = Double.parseDouble(strNum1);
        double num2 = Double.parseDouble(strNum2);
        double sum=num1+num2, difference=num1-num2, product=num1*num2, quotient=num1/num2;

        strNotification += "The sum is: "+ sum + "\n"; 
        strNotification += "The difference is: "+ difference + "\n"; 
        strNotification += "The product is: "+ product + "\n"; 
        strNotification += "The quotient is: "+ quotient + "\n"; 
    
        JOptionPane.showMessageDialog(null,strNotification,
                 "Show basic equation: ", JOptionPane. INFORMATION_MESSAGE);
        System.exit(0);
        
    }
}