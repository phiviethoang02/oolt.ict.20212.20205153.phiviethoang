package hust.soict.globalict.lab02;
import java.util.Scanner;
public class AddMatric {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter number of rows two matrices: ");
        int x = keyboard.nextInt();
        System.out.println("Enter number of columns two matrices: ");
        int y = keyboard.nextInt();
        double[][] matric1 = new double[x][y];
        double[][] matric2 = new double[x][y];
        System.out.println("Enter matrix 1: ");
        for(int i = 0; i< x; i++) {
            for(int j = 0; j < y; j++) {
                matric1[i][j] = keyboard.nextDouble();
            }
        }
        System.out.println("Enter matrix 2: ");
        for(int i = 0 ; i < x;i++) {
            for(int j = 0; j < y;j++) {
                matric2[i][j] = keyboard.nextDouble();
            }
        }
        System.out.println("The sum of 2 matrices is: ");
        for(int i = 0; i < x;i++) {
            for(int j = 0;j < y;j++) {
                System.out.print(matric1[i][j] + matric2[i][j]+" ");
            }
            System.out.print("\n");
        }
        keyboard.close();
    }
}

