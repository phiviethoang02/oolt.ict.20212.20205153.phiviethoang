package hust.soict.globalict.lab02;

import javax.swing.JOptionPane;

public class DaysOfAMonth {
    public static void main(String[] args) {
        String yearString;
        int year;
        boolean LeapYear;
        String month;
        int days = 100;
        boolean isValidMonth = true;
        boolean invalidPreviousInput = false;

        yearString = JOptionPane.showInputDialog(null, "Please enter year: ", "Year", JOptionPane.INFORMATION_MESSAGE);
        year = Integer.parseInt(yearString);
        while(year < 0) {
            yearString = JOptionPane.showInputDialog(null, "Invalid year! Please enter again: ", "Year", JOptionPane.INFORMATION_MESSAGE);
            year = Integer.parseInt(yearString);
        }    
        if(year % 4 == 0) {
            if(year % 100 == 0 && year % 400 != 0) {
                LeapYear = false;
            }else {
                LeapYear = true;
            }
        }else {
            LeapYear = false;
        }
        
        do {
            isValidMonth = true;
            String message;
            if(!invalidPreviousInput) {
                message = "Please enter month: ";
            }else {
                message = "Invalid month! Please enter again: ";
            }
            month = JOptionPane.showInputDialog(null, message, "Month", JOptionPane.INFORMATION_MESSAGE);
            switch(month) {
                case "1": case "Jan": case "Jan.":
                days = 31;
                break;
                case "2": case "Feb": case "Feb.":
                if(LeapYear) {
                    days = 29;
                }else {
                    days = 28;
                }
                break;
                case "3": case "Mar": case "Mar.":
                days = 31;
                break;
                case "4": case "Apr": case "Apr.":
                days = 30;
                break;
                case "5": case "May": case "May.":
                days = 31;
                break;
                case "6": case "June": case "Jun.":
                days = 30;
                break;
                case "7": case "July": case "Jul.":
                days = 31;
                break;
                case "8": case "Aug": case "Aug.":
                days = 31;
                break;
                case "9": case "Sep": case "Sept.":
                days = 30;
                break;
                case "10": case "Oct": case "Oct.":
                days = 31;
                break;
                case "11": case "Nov": case "Nov.":
                days = 30;
                break;
                case "12": case "Dec": case "Dec.":
                days = 31;
                break;
                default: 
                isValidMonth = false;
                invalidPreviousInput = true;
            }
        }while(!isValidMonth);

        JOptionPane.showMessageDialog(null, "The number of days is: " + Integer.toString(days), "Result",JOptionPane.INFORMATION_MESSAGE);
    }
}