import java.util.Scanner;
public class SumSortAver {
	public static void sort(double[] array){
		double temp;
		for(int i = 0; i < array.length - 1; i++) {
			for(int j = i + 1; j < array.length; j++) {
				if(array[i] >= array[j]) {
					temp = array[j];
					array[j] = array[i];
					array[i] = temp;
				}else
					continue;
			}
		}
	}
    public static void main(String[] args) {
        double sum = 0;
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter the number of elements: ");
        int n = keyboard.nextInt();
        double[] array = new double[n];
        System.out.println("Enter value for array elements: ");
        for(int i = 0; i< n; i++) {
            array[i] = keyboard.nextDouble();
            sum += array[i];
        }
        sort(array);
        System.out.println("The sum is: " + sum);
        System.out.println("The average value is: " + sum/n);
        System.out.print("Sorted array: ");
        for(int i = 0 ; i < n;i++) {
            System.out.print(array[i] + " ");
        }
        keyboard.close();
    }
}
